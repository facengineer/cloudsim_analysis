import re
import xlwt
workbook = xlwt.Workbook()

#	virtualmachine		Datacenter	Host
fileinput = open("has_been_created_in_Datacenter.output","r")
line = fileinput.readline()

sheet1 = workbook.add_sheet('sheet1',cell_overwrite_ok=True)

pattern = re.compile(r'-?[0-9]\d*')

i=1
sheet1.write(0,0,'virtualmachine')
sheet1.write(0,1,'datacenter')
sheet1.write(0,2,'host')
while line:
	#print line,
	list = pattern.findall(line)
	sheet1.write(i,0,list[2])
	sheet1.write(i,1,list[3])
	sheet1.write(i,2,list[4])
	i+=1
	line = fileinput.readline()

fileinput.close()
workbook.save('VM_TO_HOST.xls')
