import re
import xlwt
#	cloudlet	VirtualMachine
workbook = xlwt.Workbook()

fileinput = open("Broker_Sending_cloudlet.output","r")
line = fileinput.readline()

sheet1 = workbook.add_sheet('sheet1',cell_overwrite_ok=True)

pattern = re.compile(r'-?[0-9]\d*')

i=1
sheet1.write(0,0,'Cloudlet')
sheet1.write(0,1,'VirtualMachine')

while line:
	#print line,
	list = pattern.findall(line)
	print list[2:4]
	sheet1.write(i,0,list[2])
	sheet1.write(i,1,list[3])
	i+=1
	line = fileinput.readline()

fileinput.close()
workbook.save('Broker_Cloudlet.xls')
