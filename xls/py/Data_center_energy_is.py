import re
import xlwt
workbook = xlwt.Workbook()
sheet1 = workbook.add_sheet('sheet1',cell_overwrite_ok=True)

fileinput = open("Data_center_energy_is.output","r")
line = fileinput.readline()

pattern = re.compile(r'\d+\.\d+')

i = 1
sheet1.write(0,0,'time')
sheet1.write(0,1,'energy consume')
while line:
	#print line,
	list = pattern.findall(line)
	print list
	sheet1.write(i,0,list[0])
	sheet1.write(i,1,list[1])
	i += 1
	line = fileinput.readline()

fileinput.close()
workbook.save('DataCenterEnergy.xls')
