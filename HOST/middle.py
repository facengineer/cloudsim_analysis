Total_allocated_MIPS_for = open('Total_allocated_MIPS_for.output')
MIPS_for_VM_PE = open('MIPS_for_VM_PE.output')
utilization_is = open('utilization_is.output')
energy_is = open('energy_is.output')

Total_allocated_MIPS_for_line = Total_allocated_MIPS_for.readline()
MIPS_for_VM_PE_line = MIPS_for_VM_PE.readline()

while Total_allocated_MIPS_for_line:
	TaMlist = Total_allocated_MIPS_for_line.split()
	MfVlist = MIPS_for_VM_PE_line.split()
	xlslist = []

	#print "NOW TIME IS: %s, HOST ID IS: %s, VM ID is: %s, allocated MIP number is: %s, requested number is: %s, total number is: %s, percentage is: %s" % (TaMlist[0],TaMlist[2][:-1],TaMlist[8],TaMlist[12][:-1],TaMlist[15],TaMlist[19],TaMlist[20])
	xlslist.append(TaMlist[0]) #TIME 
	xlslist.append(TaMlist[2][:-1]) #HOST ID
	xlslist.append(TaMlist[8])
	xlslist.append(TaMlist[12][:-1])
	xlslist.append(TaMlist[15])
	xlslist.append(TaMlist[19])
	xlslist.append(TaMlist[20])
	
	if(len(MfVlist) < 16):
		#print "NOW TIME IS: %s, HOST ID is: %s, VM ID is: %s, PE #0 is: %s" % (MfVlist[0],MfVlist[2][:-1],MfVlist[6],MfVlist[14])
		xlslist.append(MfVlist[14])
		xlslist.append('')
        else:
                #print "NOW TIME IS: %s, HOST ID is: %s, VM ID is: %s, PE #0 is: %s, PE #1 is: %s" % (MfVlist[0],MfVlist[2][:-1],MfVlist[6],MfVlist[14][:-1],MfVlist[17][:-1])
		xlslist.append(MfVlist[14])
		xlslist.append(MfVlist[17][:-1])
	
	energy_is_line = energy_is.readline()
	while energy_is_line:
		eillist = energy_is_line.split()
		if(eillist[0] == xlslist[0] and eillist[2][:-1] == xlslist[1]):
			energyinfo = eillist[5]
		energy_is_line = energy_is.readline()


	utilization_is_line = utilization_is.readline()
	while utilization_is_line:
		uillist = utilization_is_line.split()
		if(uillist[0] == xlslist[0] and uillist[2][:-1] == xlslist[1]):
			utilizationinfo = uillist[5]
		utilization_is_line = utilization_is.readline()

	MIPS_for_VM_PE_line = MIPS_for_VM_PE.readline()
	Total_allocated_MIPS_for_line = Total_allocated_MIPS_for.readline()
	
	xlslist.append(utilizationinfo)
	xlslist.append(energyinfo)
	print xlslist

Total_allocated_MIPS_for.close()
MIPS_for_VM_PE.close()
utilization_is.close()
energy_is.close()
