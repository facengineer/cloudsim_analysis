Total_allocated_MIPS_for = open('Total_allocated_MIPS_for.output')
MIPS_for_VM_PE = open('MIPS_for_VM_PE.output')

Total_allocated_MIPS_for_line = Total_allocated_MIPS_for.readline()
MIPS_for_VM_PE_line = MIPS_for_VM_PE.readline()

while Total_allocated_MIPS_for_line:
	utilization_is = open('utilization_is.output')
	energy_is = open('energy_is.output')
	TaMlist = Total_allocated_MIPS_for_line.split()
	MfVlist = MIPS_for_VM_PE_line.split()
	xlslist = []

	xlslist.append(TaMlist[0]) #TIME 
	xlslist.append(TaMlist[2][:-1]) #HOST ID
	xlslist.append(TaMlist[8])
	xlslist.append(TaMlist[12][:-1])
	xlslist.append(TaMlist[15])
	xlslist.append(TaMlist[19])
	xlslist.append(TaMlist[20])
	
	if(len(MfVlist) < 16):
		xlslist.append(MfVlist[14])
		xlslist.append('')
        else:
		xlslist.append(MfVlist[14])
		xlslist.append(MfVlist[17][:-1])
	
	energy_is_line = energy_is.readline()
	while energy_is_line:
		eillist = energy_is_line.split()
		if(eillist[0] == xlslist[0] and eillist[2][:-1] == xlslist[1]):
			break
		energy_is_line = energy_is.readline()

	utilization_is_line = utilization_is.readline()
	while utilization_is_line:
		uillist = utilization_is_line.split()
		if(uillist[0] == xlslist[0] and uillist[2][:-1] == xlslist[1]):
			break
		utilization_is_line = utilization_is.readline()

	xlslist.append(eillist[5])
	xlslist.append(uillist[5])
	MIPS_for_VM_PE_line = MIPS_for_VM_PE.readline()
	Total_allocated_MIPS_for_line = Total_allocated_MIPS_for.readline()
	print xlslist
	utilization_is.close()
	energy_is.close()

Total_allocated_MIPS_for.close()
MIPS_for_VM_PE.close()
